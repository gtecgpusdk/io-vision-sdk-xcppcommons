/* ********************************************************************************************************* *
*
* Copyright (c) 2018 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace IoVisionSDKXCppCommonsPrimitivesTest {
    using Io::VisionSDK::XCppCommons::Primitives::Pattern;

    class PatternTest : public testing::Test {
     protected:
        void SetUp() override {
        }

        void TearDown() override {
        }
    };

    TEST_F(PatternTest, Create) {
        Pattern<char> data = {{1, 2, 3}};

        // TODO(nxa33894) implement tests
    }
}
