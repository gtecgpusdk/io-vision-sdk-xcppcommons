/* ********************************************************************************************************* *
*
* Copyright (c) 2018 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace IoVisionSDKXCppCommonsUtilsTest {
    using Io::VisionSDK::XCppCommons::Utils::Convert;

    class ConvertTest : public testing::Test {
     protected:
        void SetUp() override {
            vx_status status;

            this->context = vxCreateContext();

            this->graph = vxCreateGraph(this->context);
            status = vxGetStatus((vx_reference)this->graph);
            if (status != vx_status_e::VX_SUCCESS) {
                GTEST_FATAL_FAILURE_(": Testing VxGraph can not be initialized.");
            }
        }

        void TearDown() override {
            vxReleaseGraph(&this->graph);
            vxReleaseContext(&this->context);
        }

        void CompareImages(const cv::Mat &$img1, const cv::Mat &$img2) {
            ASSERT_NE($img1.data, nullptr);
            ASSERT_NE($img2.data, nullptr);

            ASSERT_EQ($img1.channels(), $img2.channels());
            ASSERT_EQ($img1.rows, $img2.rows);
            ASSERT_EQ($img1.cols, $img2.cols);
            ASSERT_EQ($img1.depth(), $img2.depth());

            cv::Mat planes1[$img1.channels()];  // NOLINT(runtime/arrays)
            cv::Mat planes2[$img2.channels()];  // NOLINT(runtime/arrays)
            cv::split($img1, planes1);  // NOLINT
            cv::split($img2, planes2);  // NOLINT

            cv::Mat tmpOut;
            for (int i = 0; i < $img1.channels(); i++) {
                cv::subtract(planes1[i], planes2[i], tmpOut);
                ASSERT_EQ(cv::countNonZero(tmpOut), 0);
            }
        }

     public:
        vx_context context = nullptr;
        vx_graph graph = nullptr;
    };

    TEST_F(ConvertTest, VxToCv) {
    }

    TEST_F(ConvertTest, CvToVx) {
    }
}
