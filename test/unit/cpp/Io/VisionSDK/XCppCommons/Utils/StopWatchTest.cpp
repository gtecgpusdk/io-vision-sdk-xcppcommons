/* ********************************************************************************************************* *
*
* Copyright (c) 2018 NXP
*
* SPDX-License-Identifier: BSD-3-Clause
* The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
* or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
*
* ********************************************************************************************************* */

#include <thread>
#include <chrono>

#include "../sourceFilesMap.hpp"

namespace IoVisionSDKXCppCommonsUtilsTest {
    using Io::VisionSDK::XCppCommons::Utils::StopWatch;

    class StopWatchTest : public testing::Test {
     protected:
        void SetUp() override {
        }

        void TearDown() override {
        }
    };

    TEST_F(StopWatchTest, Start) {
        StopWatch stopWatch;

        ASSERT_EQ(stopWatch.getElapsed(), 0);
        stopWatch.Start();
        ASSERT_EQ(stopWatch.getElapsed(), 0);
    }

    TEST_F(StopWatchTest, Stop) {
        StopWatch stopWatch;

        ASSERT_EQ(stopWatch.getElapsed(), 0);
        stopWatch.Stop();
        ASSERT_EQ(stopWatch.getElapsed(), 0);
    }

    TEST_F(StopWatchTest, Measure) {
        StopWatch stopWatch;

        stopWatch.Start();
        std::this_thread::sleep_for(std::chrono::milliseconds{10});
        stopWatch.Stop();
        ASSERT_GE(stopWatch.getElapsed(), 10);
    }

    TEST_F(StopWatchTest, Restart) {
        StopWatch stopWatch;

        ASSERT_EQ(stopWatch.Restart(), 0);
        std::this_thread::sleep_for(std::chrono::milliseconds{5});
        ASSERT_GE(stopWatch.Restart(), 5);
    }
}
