/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

module.exports = function ($grunt, $cwd, $args, $done) {
    const path = require('path');
    let msysRoot = $grunt.properties.binBase + '/external_modules/msys2';

    if ($grunt.properties.builderPlatform.IsWindows()) {
        const arch = '32';
        const conf = 'Debug';
        const osName = 'Windows';
        let ovxBuildName = 'ovx_build';
        let buildMsvc = true;
        let buildGcc = true;
        if ($args.length > 0) {
            if ($args[0].indexOf('msvc') < 0) {
                buildMsvc = false;
            }
            if ($args[0].indexOf('gcc') < 0) {
                buildGcc = false;
            }
        }

        if (buildMsvc && buildGcc) {
            $grunt.fail.fatal('Build OpenVX for multiple toolchains is not currently supported. ' +
                'Please select only "msvc" or "gcc" for install script attributes.');
        } else {
            if (buildGcc) {
                const postProcess = function ($basePath, $callback) {
                    const paths = $grunt.file.expand($basePath + '/**/*.dll');
                    let mainPath = "";

                    const rename = function ($index) {
                        if ($index < paths.length) {
                            let name = path.basename(paths[$index]);
                            if (name.indexOf('lib') === 0) {
                                $grunt.fileSystemHandler.Rename(paths[$index], paths[$index].replace(name, name.substr(3)), function () {
                                    if (name === 'libopenvx.dll') {
                                        mainPath = path.dirname(paths[$index]);
                                    }
                                    rename($index + 1);
                                })
                            } else {
                                rename($index + 1);
                            }
                        } else {
                            if (mainPath.length === 0) {
                                $grunt.fail.fatal('Can not localize directory with openvx.dll.');
                            }
                            const envCfg = {
                                cwd: mainPath,
                                env: {PATH: msysRoot + '/usr/bin'}
                            };

                            $grunt.terminalHandler.Spawn('gendef', ['openvx.dll'], envCfg, function ($exitCode) {
                                if ($exitCode === 0) {
                                    $grunt.terminalHandler.Spawn('dlltool', [
                                        '-d', 'openvx.def',
                                        '-D', 'openvx.dll',
                                        '-y', 'libopenvx.dll.a'
                                    ], envCfg, function ($exitCode) {
                                        $grunt.fileSystemHandler.Delete(mainPath + "/openvx.def", function () {
                                            if ($exitCode === 0) {
                                                $grunt.fileSystemHandler.Copy(mainPath + "/libopenvx.dll.a", mainPath + "/../lib/libopenvx.dll.a", function ($status) {
                                                    $grunt.fileSystemHandler.Delete(mainPath + "/libopenvx.dll.a", function () {
                                                        if ($status) {
                                                            $callback();
                                                        } else {
                                                            $grunt.fail.fatal("Can not replace default interface lib with delay loaded one.");
                                                        }
                                                    });
                                                });
                                            } else {
                                                $grunt.fail.fatal('Can not generate delay loaded interface for openvx.dll');
                                            }
                                        });
                                    });
                                } else {
                                    $grunt.fail.fatal('Can not generage def file for openvx.dll.');
                                }
                            });
                        }
                    };

                    rename(0);
                };

                if (!$grunt.fileSystemHandler.Exists($cwd + '/build-gcc/' + ovxBuildName)) {
                    const replaceApiCall = function ($callback) {
                        $grunt.fileSystemHandler.Read($cwd + '/include/VX/vx_types.h', function ($data) {
                            let res = $data;
                            if (res.indexOf('#define VX_API_CALL //__stdcall') === -1) {
                                res = res.replace('#define VX_API_CALL __stdcall', '#define VX_API_CALL //__stdcall');
                            }
                            if (res.indexOf('#define VX_CALLBACK //__stdcall') === -1) {
                                res = res.replace('#define VX_CALLBACK __stdcall', '#define VX_CALLBACK //__stdcall');
                            }

                            $grunt.fileSystemHandler.Write($cwd + '/include/VX/vx_types.h', res, false, function ($status) {
                                $callback($status);
                            });
                        });
                    };

                    const prepare = function ($callback) {
                        $grunt.fileSystemHandler.Read($cwd + '/CMakeLists.txt', function ($data) {
                            let res = $data;
                            if (res.indexOf('set(WIN32 false CACHE INTERNAL \"\")\n') === -1) {
                                res = res.replace('cmake_minimum_required(VERSION 2.8.9)',
                                    'cmake_minimum_required(VERSION 2.8.9)\n' +
                                    'set(WIN32 false CACHE INTERNAL \"\")\n' +
                                    'set(CYGWIN true CACHE INTERNAL \"\")\n');
                            }

                            if ($grunt.project.dependencies['openvx'].version === "1.1") {
                                if (res.indexOf('#add_subdirectory( sample-c++ )') === -1) {
                                    res = res.replace('add_subdirectory( sample-c++ )', '#add_subdirectory( sample-c++ )');
                                }
                            }

                            $grunt.fileSystemHandler.Write($cwd + "/CMakeLists.txt", res, false, function () {
                                replaceApiCall(function () {
                                    $grunt.fileSystemHandler.Exists($cwd + "/build-gcc", function ($status) {
                                        if (!$status) {
                                            $grunt.fileSystemHandler.CreateDirectory($cwd + '/build-gcc', function ($status) {
                                                $callback($status);
                                            });
                                        } else {
                                            $callback(true);
                                        }
                                    });
                                });
                            });
                        });
                    };

                    prepare(function ($status) {
                        if ($status === true) {
                            $grunt.terminalHandler.Spawn(
                                'sh.exe', ['-c \"', 'cmake', '-DCMAKE_BUILD_TYPE=' + conf,
                                    '-DCMAKE_INSTALL_PREFIX=' + '$(pwd)/install/' + osName + '/' + arch + '/' + conf,
                                    '-DBUILD_X64=0', '-G\\"MinGW Makefiles\\" -DCMAKE_SH=\\"CMAKE_SH-NOTFOUND\\" ' +
                                    '-DCMAKE_CXX_FLAGS=\\"-static-libstdc++\\" -DCMAKE_C_FLAGS=\\"-static-libgcc\\"',
                                    '..',
                                    '\"'],
                                {
                                    cwd: $cwd + '/build-gcc',
                                    env: {
                                        PATH: msysRoot + '/usr/bin',
                                        NUMBER_OF_PROCESSORS: $grunt.properties.builderPlatform.getCores()
                                    }
                                },
                                function (exitCode) {
                                    if (exitCode !== 0) {
                                        $grunt.fail.fatal('Build of openvx package failed.');
                                    } else {
                                        $grunt.terminalHandler.Spawn(
                                            'sh.exe', ['-c \"', 'cmake --build . --target install -- -j' + $grunt.properties.builderPlatform.getCores(), '\"'],
                                            {
                                                cwd: $cwd + '/build-gcc',
                                                env: {PATH: msysRoot + '/usr/bin'}
                                            },
                                            function (exitCode) {
                                                $grunt.fileSystemHandler.CreateDirectory($cwd + '/build-gcc/' + ovxBuildName, function ($success) {
                                                    if ($success) {
                                                        const outputPath = $cwd + '/build-gcc/' + ovxBuildName;
                                                        $grunt.fileSystemHandler.Copy($cwd + '/build-gcc/install/' + osName + '/' + arch + '/' + conf,
                                                            outputPath,
                                                            function ($success) {
                                                                if ($success) {
                                                                    postProcess(outputPath, function () {
                                                                        $grunt.log.writeln('Openvx build succeed.');
                                                                        $done();
                                                                    });
                                                                } else {
                                                                    $grunt.fail.fatal('Copy of openvx build dir failed.');
                                                                }
                                                            });
                                                    } else {
                                                        $grunt.fail.fatal('Create of ovx_build dir failed.');
                                                    }
                                                })
                                            });
                                    }
                                });
                        } else {
                            $grunt.fail.fatal('Unable to prepare openvx cmakelists or build folder.');
                        }
                    });
                } else {
                    $grunt.log.writeln('Openvx already built.');
                    $done();
                }
            }
            if (buildMsvc) {
                const buildPath = $cwd + '/build-msvc';

                const process = function () {
                    $grunt.terminalHandler.Execute(
                        'cmake',
                        [
                            '-G"Visual Studio 14 2015 Win64"',
                            '-DCMAKE_BUILD_TYPE=Release',
                            '-DBUILD_X64=1',
                            '-DCMAKE_INSTALL_PREFIX=ovx_build',
                            '..'
                        ],
                        buildPath,
                        function ($exitCode, $std) {
                            if ($exitCode === 0) {
                                $grunt.terminalHandler.Spawn('cmake', ['--build . --target install'], buildPath, function ($exitCode, $std) {
                                    if ($exitCode === 0) {
                                        $done();
                                    } else {
                                        $grunt.fail.fatal($std[0] + $std[1]);
                                    }
                                });
                            } else {
                                $grunt.fail.fatal($std[0] + $std[1]);
                            }
                        });
                };
                $grunt.fileSystemHandler.Exists(buildPath, function ($status) {
                    if (!$status) {
                        $grunt.fileSystemHandler.CreateDirectory(buildPath, function ($status) {
                            if ($status) {
                                process();
                            } else {
                                $grunt.fail.fatal('Can not create build directory.');
                            }
                        });
                    } else {
                        $grunt.fileSystemHandler.Exists(buildPath + '/ovx_build', function ($status) {
                            if (!$status) {
                                process();
                            } else {
                                $grunt.log.writeln('OpenVX build exists. Skipped.');
                                $done();
                            }
                        });
                    }
                });
            }
        }
    } else {
        const arch = '64';
        const conf = 'Release';
        const osName = 'Linux';
        const buildPath = $cwd + '/build-gcc/ovx_build';
        if (!$grunt.fileSystemHandler.Exists(buildPath)) {
            const prepare = function ($callback) {
                $grunt.fileSystemHandler.Read($cwd + '/CMakeLists.txt', function ($data) {
                    let res = $data;

                    if ($grunt.project.dependencies['openvx'].version === "1.1") {
                        res = res.replace('add_subdirectory( sample-c++ )', '#add_subdirectory( sample-c++ )');
                    }

                    $grunt.fileSystemHandler.Write($cwd + "/CMakeLists.txt", res, false, function ($status) {
                        $grunt.fileSystemHandler.Exists($cwd + "/build-gcc", function ($status) {
                            if (!$status) {
                                $grunt.fileSystemHandler.CreateDirectory($cwd + '/build-gcc', function ($status) {
                                    $callback($status);
                                });
                            } else {
                                $callback(true);
                            }
                        });
                    });
                });
            };

            prepare(function ($status) {
                if ($status === true) {
                    $grunt.terminalHandler.Spawn(
                        'python', ['Build.py', '--os=' + osName, '--arch=' + arch, '--conf=' + conf, '--c=gcc-5', '--cpp=g++-5'],
                        $cwd,
                        function (exitCode) {
                            if (exitCode !== 0) {
                                $grunt.fail.fatal('Build of openvx package failed.');
                            } else {
                                $grunt.fileSystemHandler.CreateDirectory(buildPath, function ($success) {
                                    if ($success) {
                                        $grunt.fileSystemHandler.Copy($cwd + '/install/' + osName + '/x' + arch + '/' + conf,
                                            buildPath,
                                            function ($success) {
                                                if ($success) {
                                                    $grunt.log.writeln('openvx build succeed.');
                                                    $done();
                                                } else {
                                                    $grunt.fail.fatal('Copy of openvx build dir failed.');
                                                }
                                            });
                                    } else {
                                        $grunt.fail.fatal('Create of ovx_build dir failed.');
                                    }
                                })
                            }
                        });
                } else {
                    $grunt.fail.fatal('Unable to prepare openvx cmakelists or build folder.');
                }
            })
        } else {
            $grunt.log.writeln('openvx already built.');
            $done();
        }
    }
};
