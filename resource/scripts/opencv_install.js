/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

module.exports = function ($grunt, $cwd, $args, $done) {
    const os = require('os');

    if ($grunt.properties.builderPlatform.IsWindows()) {
        let buildMsvc = true;
        let buildGcc = true;
        if ($args.length > 0) {
            if ($args[0].indexOf('msvc') < 0) {
                buildMsvc = false;
            }
            if ($args[0].indexOf('gcc') < 0) {
                buildGcc = false;
            }
        }

        if (buildMsvc && buildGcc) {
            $grunt.fail.fatal('Build OpenCV for multiple toolchains is not currently supported. ' +
                'Please select only "msvc" or "gcc" for install script attributes.');
        } else {
            if (buildGcc) {
                let msysRoot = $grunt.properties.binBase + '/external_modules/msys2';
                const scriptName = 'tmpOpenCVInstallScript.sh';
                const buildPath = $cwd + '/build-gcc';
                const scriptData = '' +
                    '#!/bin/bash' + os.EOL +
                    'cmake -G"MinGW Makefiles" ' +
                    '-DPYTHON_EXECUTABLE="' + $grunt.fileSystemHandler.NormalizePath($grunt.properties.binBaseUnix + '/external_modules/python36/python.exe') + '" ' +
                    '-DCMAKE_INSTALL_PREFIX=ocv_build ' +
                    '-DWITH_OPENEXR=OFF ' +
                    '-DWITH_JPEG=ON ' +
                    '-DWITH_TIFF=ON ' +
                    '-DWITH_PNG=ON ' +
                    '-DWITH_JASPER=ON ' +
                    '-DBUILD_EXAMPLES=OFF ' +
                    '-DBUILD_DOCS=OFF ' +
                    '-DBUILD_PERF_TESTS=OFF ' +
                    '-DBUILD_TESTS=OFF ' +
                    '-DBUILD_SHARED_LIBS=OFF ' +
                    '-DBUILD_WITH_DEBUG_INFO=OFF ' +
                    '-DOPENCV_BUILD_3RDPARTY_LIBS=ON ' +
                    '-DCMAKE_SH="CMAKE_SH-NOTFOUND" ..' + os.EOL +
                    'cmake --build . --target install -- -j' + $grunt.properties.builderPlatform.getCores() + os.EOL;

                const process = function () {
                    if ($grunt.fileSystemHandler.Write(buildPath + '/' + scriptName, scriptData, false)) {
                        const cmakePath = $cwd + '/CMakeLists.txt';
                        let cmakeData = $grunt.fileSystemHandler.Read(cmakePath)
                            .replace('include(cmake/OpenCVDetectPython.cmake)', '#include(cmake/OpenCVDetectPython.cmake)');
                        $grunt.fileSystemHandler.Rename(cmakePath, cmakePath + '.backup');
                        if ($grunt.fileSystemHandler.Write(cmakePath, cmakeData, false)) {
                            $grunt.terminalHandler.Spawn(
                                'sh.exe', ['./' + scriptName],
                                {
                                    cwd: buildPath,
                                    env: {
                                        PATH: msysRoot + '/usr/bin;'
                                    }
                                },
                                function ($exitCode) {
                                    if ($exitCode !== 0) {
                                        $grunt.fail.fatal('Build of opencv package failed.');
                                    } else {
                                        $grunt.log.writeln('opencv build succeed.');
                                        $grunt.fileSystemHandler.Delete(buildPath + '/' + scriptName);
                                        $done();
                                    }
                                });
                        } else {
                            $grunt.fail.fatal('Build of opencv package failed: unable to apply path on CMakeLists.txt.');
                        }
                    } else {
                        $grunt.fail.fatal('Build of opencv package failed: unable to write script file.');
                    }
                };

                if (!$grunt.fileSystemHandler.Exists(buildPath)) {
                    if ($grunt.fileSystemHandler.CreateDirectory(buildPath)) {
                        process();
                    } else {
                        $grunt.fail.fatal('Can not create build directory.');
                    }
                } else if (!$grunt.fileSystemHandler.Exists(buildPath + '/ocv_build')) {
                    process();
                } else {
                    $grunt.log.writeln('OpenCV build exists. Skipped.');
                    $done();
                }
            }
            if (buildMsvc) {
                const buildPath = $cwd + '/build-msvc';
                const process = function () {
                    $grunt.terminalHandler.Execute(
                        'cmake',
                        [
                            '-G"Visual Studio 14 2015 Win64"',
                            '-DPYTHON_EXECUTABLE="' + $grunt.fileSystemHandler.NormalizePath($grunt.properties.binBaseUnix + '/external_modules/python36/python.exe') + '"',
                            '-DCMAKE_BUILD_TYPE=Release',
                            '-DCMAKE_INSTALL_PREFIX=ocv_build',
                            '-DWITH_OPENEXR=OFF',
                            '-DWITH_JPEG=ON',
                            '-DWITH_TIFF=ON',
                            '-DWITH_PNG=ON',
                            '-DWITH_JASPER=ON',
                            '-DBUILD_EXAMPLES=OFF',
                            '-DBUILD_DOCS=OFF',
                            '-DBUILD_PERF_TESTS=OFF',
                            '-DBUILD_TESTS=OFF',
                            '-DBUILD_SHARED_LIBS=OFF',
                            '-DOPENCV_BUILD_3RDPARTY_LIBS=ON',
                            '..'
                        ],
                        {
                            cwd: buildPath,
                            env: {
                                PATH: $grunt.fileSystemHandler.NormalizePath($grunt.properties.binBaseUnix + '/external_modules/python36') + ';' +
                                $grunt.fileSystemHandler.NormalizePath($grunt.properties.binBaseUnix + '/external_modules/python36/Scripts')
                            }
                        },
                        function ($exitCode, $std) {
                            if ($std[0].indexOf('-- Generating done') >= 0) {
                                $grunt.terminalHandler.Spawn('cmake', ['--build . --target install'], buildPath, function ($exitCode, $std) {
                                    if ($exitCode === 0) {
                                        $done();
                                    } else {
                                        $grunt.fail.fatal($std[0] + $std[1]);
                                    }
                                });
                            } else {
                                $grunt.fail.fatal($std[0] + $std[1]);
                            }
                        });
                };

                if (!$grunt.fileSystemHandler.Exists(buildPath)) {
                    if ($grunt.fileSystemHandler.CreateDirectory(buildPath)) {
                        process();
                    } else {
                        $grunt.fail.fatal('Can not create build directory.');
                    }
                } else if (!$grunt.fileSystemHandler.Exists(buildPath + '/ocv_build')) {
                    process();
                } else {
                    $grunt.log.writeln('OpenCV build exists. Skipped.');
                    $done();
                }
            }
        }
    } else {
        const buildPath = $cwd + '/build-gcc';
        const ffmpegBuildPath = buildPath + '/ffmpeg_build';
        const process = function () {
            const installGTK = function ($callback) {
                $grunt.terminalHandler.Spawn('dpkg', ['-l', 'libgtk2.0-0'], null, function ($exitCode, $std) {
                    if ($exitCode !== 0) {
                        $grunt.fail.fatal('Validation of GTK package failed.');
                    } else if ($std[0].indexOf('gtk') === -1) {
                        $grunt.terminalHandler.Spawn('sudo apt-get install gtk2.0 libgtk2.0-dev -y', [], null, function ($exitCode) {
                            if ($exitCode !== 0) {
                                $grunt.fail.fatal('Unable to install GTK package.');
                            } else {
                                $callback();
                            }
                        });
                    } else {
                        $callback();
                    }
                });
            };
            const installFFmpeg = function ($callback) {
                if (!$grunt.fileSystemHandler.Exists(ffmpegBuildPath)) {
                    $grunt.fileSystemHandler.Download('http://ffmpeg.org/releases/ffmpeg-3.4.2.tar.bz2', function ($headers, $path) {
                        $grunt.fileSystemHandler.Unpack($path, {
                            output: ffmpegBuildPath
                        }, function () {
                            $grunt.fileSystemHandler.Delete($path);
                            $grunt.terminalHandler.Spawn(
                                'chmod +x configure && ' +
                                'chmod +x ffbuild/version.sh && ' +
                                'chmod +x ffbuild/libversion.sh && ' +
                                'chmod +x ffbuild/pkgconfig_generate.sh && ' +
                                './configure --prefix=' + ffmpegBuildPath + '/build --enable-gpl --enable-nonfree --enable-postproc --enable-version3 --enable-shared --disable-x86asm && ' +
                                'make -j4 && ' +
                                'make install', [],
                                ffmpegBuildPath,
                                function ($exitCode) {
                                    if ($exitCode !== 0) {
                                        $grunt.fail.fatal('Build of ffmpeg package failed.');
                                    } else {
                                        $callback();
                                    }
                                });
                        });
                    });
                } else {
                    $callback();
                }
            };
            installGTK(function () {
                installFFmpeg(function () {
                    const scriptName = 'tmpOpenCVInstallScript.sh';
                    const scriptData = '' +
                        '#!/bin/bash' + os.EOL +
                        'PATH="' + ffmpegBuildPath + '/build/lib:' + ffmpegBuildPath + '/build/bin:$PATH"' + os.EOL +
                        'cmake .. ' +
                        '-DCMAKE_INSTALL_PREFIX=ocv_build ' +
                        '-DWITH_OPENEXR=OFF ' +
                        '-DWITH_JPEG=ON ' +
                        '-DWITH_TIFF=ON ' +
                        '-DWITH_PNG=ON ' +
                        '-DWITH_JASPER=ON ' +
                        '-DWITH_FFMPEG=ON ' +
                        '-DWITH_TBB=OFF ' +
                        '-DWITH_LIBV4L=OFF ' +
                        '-DWITH_GSTREAMER=OFF ' +
                        '-DWITH_1394=OFF ' +
                        '-DWITH_QT=OFF ' +
                        '-DWITH_OPENGL=ON ' +
                        '-DWITH_VTK=ON ' +
                        '-DBUILD_EXAMPLES=OFF ' +
                        '-DBUILD_DOCS=OFF ' +
                        '-DBUILD_PERF_TESTS=OFF ' +
                        '-DBUILD_TESTS=OFF ' +
                        '-DBUILD_SHARED_LIBS=ON ' + os.EOL +
                        'cmake --build . --target install -- -j' + $grunt.properties.builderPlatform.getCores() + os.EOL +
                        '';
                    if ($grunt.fileSystemHandler.Write(buildPath + '/' + scriptName, scriptData, false)) {
                        $grunt.terminalHandler.Spawn(
                            'chmod +x ' + scriptName + ' && ' + './' + scriptName, [], buildPath,
                            function ($exitCode) {
                                if ($exitCode !== 0) {
                                    $grunt.fail.fatal('Build of opencv package failed.');
                                } else {
                                    $grunt.log.writeln('opencv build succeed.');
                                    $grunt.fileSystemHandler.Delete(scriptName);
                                    $done();
                                }
                            });
                    } else {
                        $grunt.fail.fatal('Build of opencv package failed: unable to write script file.');
                    }
                });
            });
        };

        if (!$grunt.fileSystemHandler.Exists(buildPath)) {
            if ($grunt.fileSystemHandler.CreateDirectory(buildPath)) {
                process();
            } else {
                $grunt.fail.fatal('Can not create build directory.');
            }
        } else if (!$grunt.fileSystemHandler.Exists(buildPath + '/ocv_build')) {
            process();
        } else {
            $grunt.log.writeln('OpenCV build exists. Skipped.');
            $done();
        }
    }
};
