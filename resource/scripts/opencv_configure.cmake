# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(OPENCV_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running OPENCV_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")

    PARSE_ARGS("${attributes}")

    set(OPENCV_INCLUDE_DIRS)
    set(OPENCV_LIBRARIES)

    set(_BASE_PATH ${path})
    if (NOT IS_ABSOLUTE ${_BASE_PATH})
        set(_BASE_PATH ${CMAKE_SOURCE_DIR}/${_BASE_PATH})
    endif ()

    if (TOOLCHAIN_TYPE STREQUAL "gcc")
        set(OpenCV_DIR "${_BASE_PATH}/build-gcc")
    else ()
        set(OpenCV_DIR "${_BASE_PATH}/build-msvc")
    endif ()

    find_package(OpenCV COMPONENTS ${ARGS_ITEMS} REQUIRED)

    set(OPENCV_INCLUDE_DIRS ${OpenCV_INCLUDE_DIRS})
    set(OPENCV_LIBRARIES ${OpenCV_LIBS})

    target_include_directories(${app_target} SYSTEM PUBLIC ${OpenCV_INCLUDE_DIRS})
    target_include_directories(${test_target} SYSTEM PUBLIC ${OpenCV_INCLUDE_DIRS})
    target_include_directories(${test_lib_target} SYSTEM PUBLIC ${OpenCV_INCLUDE_DIRS})

    target_link_libraries(${app_target} ${OPENCV_LIBRARIES})
    target_link_libraries(${test_target} ${OPENCV_LIBRARIES})
    target_link_libraries(${test_lib_target} ${OPENCV_LIBRARIES})

endmacro()
