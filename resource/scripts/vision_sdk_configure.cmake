# * ********************************************************************************************************* *
# *
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(VISION_SDK_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running VISION_SDK_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")

    execute_process(
            COMMAND where msvcp120d.dll
            RESULT_VARIABLE VC12_EXISTS)

    set(TOOLCHAIN_ENABLED OFF)

    if (${attributes} STREQUAL "vc12d")
        if (${VC12_EXISTS} STREQUAL "0")
            message(STATUS "Using detected vc120d toolchain.")
            set(TOOLCHAIN_ENABLED ON)
        endif ()
    elseif (${attributes} STREQUAL "vc14d")
        if (${VC12_EXISTS} STREQUAL "1")
            message(STATUS "Using detected vc140d toolchain.")
            set(TOOLCHAIN_ENABLED ON)
        endif ()
    elseif (${attributes} STREQUAL "gcc")
        set(TOOLCHAIN_ENABLED ON)
    else ()
        message(FATAL_ERROR "Supported are only vc12d or vc14d toolchain values.")
    endif ()

    if (TOOLCHAIN_ENABLED)
        if (WIN32)
            set(VISION_SDK_PATH ${path}/libs)
            message("VISION_SDK path: ${VISION_SDK_PATH}")

            set(VISION_SDK_INCLUDES
                    ${VISION_SDK_PATH}/opencv/include
                    ${VISION_SDK_PATH}/openvx/include
                    )

            set(VISION_SDK_LIBRARIES_NAMES
                    openvx
                    opencv_highgui2411d
                    opencv_imgproc2411d
                    opencv_core2411d
                    opencv_objdetect2411d
                    )

            set(VISION_SDK_LIBRARIES_DIRS
                    ${VISION_SDK_PATH}/opencv/lib
                    ${VISION_SDK_PATH}/openvx/lib
                    )

            foreach (libPath ${VISION_SDK_LIBRARIES_DIRS})
                foreach (item ${VISION_SDK_LIBRARIES_NAMES})
                    set(LIB ${libPath}/${item}.lib)
                    if (EXISTS ${LIB})
                        set(VISION_SDK_LIBRARIES ${VISION_SDK_LIBRARIES} ${LIB})
                    endif ()
                endforeach (item)
            endforeach (libPath)
        elseif (UNIX)
            set(VISION_SDK_PATH ${path}/libs)
            message("VISION_SDK path: ${VISION_SDK_PATH}")

            set(VISION_SDK_INCLUDES
                    ${VISION_SDK_PATH}/opencv/include
                    ${VISION_SDK_PATH}/openvx/include
                    )

            set(VISION_SDK_LIBRARIES_NAMES
                    libopenvx.so
                    libopencv_highgui
                    libopencv_objdetect
                    libopencv_calib3d
                    libopencv_features2d
                    libopencv_imgproc
                    libopencv_core
                    )

            set(VISION_SDK_LIBRARIES_DIRS
                    ${VISION_SDK_PATH}/opencv/lib
                    ${VISION_SDK_PATH}/openvx/lib
                    )
            foreach (libPath ${VISION_SDK_LIBRARIES_DIRS})
                message("libpath " ${libPath})
                foreach (item ${VISION_SDK_LIBRARIES_NAMES})
                    message("names " ${item})
                    set(LIB ${libPath}/${item}.a)
                    if (EXISTS ${LIB})
                        set(VISION_SDK_LIBRARIES ${VISION_SDK_LIBRARIES} ${LIB})
                        message(STATUS "Library ${LIB} exists")
                    endif ()
                endforeach (item)
            endforeach (libPath)
        else ()
            message(FATAL_ERROR "Platform not supported.")
        endif ()

        message("VISION_SDK library found: " ${VISION_SDK_LIBRARIES})
        message("VISION_SDK library paths: " ${VISION_SDK_LIBRARIES_DIRS})

        target_include_directories(${app_target} SYSTEM PUBLIC ${VISION_SDK_INCLUDES})
        target_include_directories(${test_target} SYSTEM PUBLIC ${VISION_SDK_INCLUDES})

        message(STATUS "libs: ${VISION_SDK_LIBRARIES}")
        target_link_libraries(${app_target} ${VISION_SDK_LIBRARIES})
        target_link_libraries(${test_target} ${VISION_SDK_LIBRARIES})
    else ()
        message(STATUS "Configuration skipped due to toolchain mismach. ${TOOLCHAIN_ENABLED} : ${attributes}")
    endif ()

endmacro()
