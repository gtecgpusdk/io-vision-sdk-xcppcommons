# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(OPENVX_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running OPENVX_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")

    PARSE_ARGS("${attributes}")

    set(_BASE_PATH ${path})
    if (NOT IS_ABSOLUTE ${_BASE_PATH})
        set(_BASE_PATH ${CMAKE_SOURCE_DIR}/${_BASE_PATH})
    endif ()

    if (TOOLCHAIN_TYPE STREQUAL "gcc")
        set(OpenVX_DIR "${_BASE_PATH}/build-gcc/ovx_build")
    else ()
        set(OpenVX_DIR "${_BASE_PATH}/build-msvc/ovx_build")
    endif ()

    set(OPENVX_INCLUDE_DIRS ${OpenVX_DIR}/include)
    set(OPENVX_LIBRARIES )

    set(OPENVX_LIB_DIR ${OpenVX_DIR}/lib)
    set(OPENVX_BIN_DIR ${OpenVX_DIR}/bin)

    message("looking in: ${OPENVX_LIB_DIR}")

    set(OPENVX_LIBS_NAMES
            openvx
            )

    foreach (item ${OPENVX_LIBS_NAMES})
        find_library(OPENVX_LIB_${item}_FOUND ${item} ${OPENVX_LIB_DIR} ${OPENVX_BIN_DIR})
        if (OPENVX_LIB_${item}_FOUND)
            set(OPENVX_LIBRARIES ${OPENVX_LIB_${item}_FOUND} ${OPENVX_LIBRARIES})
            message(STATUS "found: ${OPENVX_LIB_${item}_FOUND}")
        else ()
            message(FATAL_ERROR "OpenVX lib NOT Found: ${item}")
        endif ()
    endforeach ()

    target_link_libraries(${app_target} ${OPENVX_LIBRARIES})
    target_link_libraries(${test_target} ${OPENVX_LIBRARIES})
    target_link_libraries(${test_lib_target} ${OPENVX_LIBRARIES})

    target_include_directories(${app_target} PUBLIC ${OPENVX_INCLUDE_DIRS})
    target_include_directories(${test_target} PUBLIC ${OPENVX_INCLUDE_DIRS})
    target_include_directories(${test_lib_target} PUBLIC ${OPENVX_INCLUDE_DIRS})

endmacro()
