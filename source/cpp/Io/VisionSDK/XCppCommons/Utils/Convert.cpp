/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::VisionSDK::XCppCommons::Utils::Convert;

vx_status Convert::VxToCv(const vx_image &$input, cv::Mat &$output, const vx_context &$context) {
    vx_status status;

    vx_df_image vxImageType = 0;
    vx_uint32 width = 0, height = 0;

#ifdef VX_VERSION_1_1
    status = vxQueryImage($input, vx_image_attribute_e::VX_IMAGE_FORMAT, &vxImageType, sizeof(vx_df_image));
    if (status == vx_status_e::VX_SUCCESS) {
        status = vxQueryImage($input, vx_image_attribute_e::VX_IMAGE_WIDTH, &width, sizeof(vx_uint32));
    }
    if (status == vx_status_e::VX_SUCCESS) {
        status = vxQueryImage($input, vx_image_attribute_e::VX_IMAGE_HEIGHT, &height, sizeof(vx_uint32));
    }
#elif VX_VERSION_1_0
    status = vxQueryImage($input, vx_image_attribute_e::VX_IMAGE_ATTRIBUTE_FORMAT, &vxImageType, sizeof(vx_df_image));
    if (status == vx_status_e::VX_SUCCESS) {
        status = vxQueryImage($input, vx_image_attribute_e::VX_IMAGE_ATTRIBUTE_WIDTH, &width, sizeof(vx_uint32));
    }
    if (status == vx_status_e::VX_SUCCESS) {
        status = vxQueryImage($input, vx_image_attribute_e::VX_IMAGE_ATTRIBUTE_HEIGHT, &height, sizeof(vx_uint32));
    }
#endif

    if (status == vx_status_e::VX_SUCCESS) {
        if ((width > 0) && (height > 0)) {
            int cvColor = 0;

            switch (vxImageType) {
                case vx_df_image_e::VX_DF_IMAGE_U8: {
                    cvColor = CV_8UC1;
                    break;
                }
                case vx_df_image_e::VX_DF_IMAGE_U16: {
                    cvColor = CV_16UC1;
                    break;
                }
                case vx_df_image_e::VX_DF_IMAGE_S16: {
                    cvColor = CV_16SC1;
                    break;
                }
                case vx_df_image_e::VX_DF_IMAGE_U32:
                case vx_df_image_e::VX_DF_IMAGE_S32: {
                    cvColor = CV_32SC1;
                    break;
                }
                case vx_df_image_e::VX_DF_IMAGE_RGB: {
                    cvColor = CV_8UC3;
                    break;
                }
                case vx_df_image_e::VX_DF_IMAGE_RGBX: {
                    cvColor = CV_8UC4;
                    break;
                }
                default: {
                    status = vx_status_e::VX_FAILURE;
                }
            }

            if (status == vx_status_e::VX_SUCCESS) {
                cv::Mat output(height, width, cvColor);
                vx_rectangle_t rect{
                        .start_x = 0,
                        .start_y = 0,
                        .end_x = width,
                        .end_y = height
                };
                vx_imagepatch_addressing_t addressing{
                        .dim_x = static_cast<vx_uint32>(output.cols),
                        .dim_y = static_cast<vx_uint32>(output.rows),
                        .stride_x = static_cast<vx_int32>(output.elemSize()),
                        .stride_y = static_cast<vx_int32>(output.step)  // or cvImg.step.p[0]... check difference!!
                };
                vx_uint8 *data = output.data;

#ifdef VX_VERSION_1_1
                status = vxCopyImagePatch($input, &rect, 0, &addressing, data, VX_READ_ONLY, VX_MEMORY_TYPE_HOST);

#elif VX_VERSION_1_0
                status = vxAccessImagePatch($input, &rect, 0, &addressing, reinterpret_cast<void **>(&data), VX_READ_ONLY);
                if (status == vx_status_e::VX_SUCCESS) {
                    status = vxCommitImagePatch($input, &rect, 0, &addressing, data);
                }
#else
#error Conversion from OpenVX to OpenCV is not implemented for that versiong of OpenVX.
#endif

                if (status == vx_status_e::VX_SUCCESS) {
                    if (cvColor == CV_8UC3) {
                        cv::cvtColor(output, $output, CV_RGB2BGR);
                    } else if (cvColor == CV_8UC4) {
                        cv::cvtColor(output, $output, CV_RGBA2BGRA);
                    } else {
                        $output = output;
                    }
                }
            }
        } else {
            status = vx_status_e::VX_FAILURE;
        }
    }

    return status;
}

vx_status Convert::CvToVx(const cv::Mat &$input, vx_image &$output, const vx_context &$context, bool $copy) {
    vx_status status = vx_status_e::VX_SUCCESS;
    if ($input.data != nullptr) {
        vx_imagepatch_addressing_t addressing{
                .dim_x = static_cast<vx_uint32>($input.cols),
                .dim_y = static_cast<vx_uint32>($input.rows),
                .stride_x = static_cast<vx_int32>($input.elemSize()),
                .stride_y = static_cast<vx_int32>($input.step)  // or cvImg.step.p[0]... check difference!!
        };

        cv::Mat input;
        vx_df_image vxColor = vx_df_image_e::VX_DF_IMAGE_U8;
        if ($input.channels() == 4) {
            vxColor = vx_df_image_e::VX_DF_IMAGE_RGBX;
            cv::cvtColor($input, input, CV_BGRA2RGBA, 0);
        } else if ($input.channels() == 3) {
            vxColor = vx_df_image_e::VX_DF_IMAGE_RGB;
            cv::cvtColor($input, input, CV_BGR2RGB, 0);
        } else if ($input.channels() == 1) {
            input = $input;
            switch ($input.depth()) {
                case CV_8U:
                case CV_8S: {
                    vxColor = vx_df_image_e::VX_DF_IMAGE_U8;
                    break;
                }
                case CV_16U: {
                    vxColor = vx_df_image_e::VX_DF_IMAGE_U16;
                    break;
                }
                case CV_16S: {
                    vxColor = vx_df_image_e::VX_DF_IMAGE_S16;
                    break;
                }
                case CV_32S: {
                    vxColor = vx_df_image_e::VX_DF_IMAGE_S32;
                    break;
                }
                default: {
                    status = vx_status_e::VX_FAILURE;
                }
            }
        } else {
            status = vx_status_e::VX_FAILURE;
        }

        if (status == vx_status_e::VX_SUCCESS) {
            // copy data from cv::Mat object to memory area used by vx_image handle.
            auto *data = new vx_uint8[input.dataend - input.datastart];
            memcpy(data, input.data, input.dataend - input.datastart);

            if (!$copy || $output == nullptr) {
#ifdef VX_VERSION_1_1
                $output = vxCreateImageFromHandle($context, vxColor, &addressing, reinterpret_cast<void **>(&data),
                                                  vx_memory_type_e::VX_MEMORY_TYPE_HOST);
#elif VX_VERSION_1_0
                $output = vxCreateImageFromHandle($context, vxColor, &addressing, reinterpret_cast<void **>(&data),
                                                  VX_IMPORT_TYPE_HOST);
#endif
                if ($output == nullptr) {
                    status = vx_status_e::VX_FAILURE;
                }
            } else {
                vx_rectangle_t rect;
                status = vxGetValidRegionImage($output, &rect);
                if (status == vx_status_e::VX_SUCCESS) {
#ifdef VX_VERSION_1_1
                    status = vxCopyImagePatch($output, &rect, 0, &addressing, data, VX_WRITE_ONLY, VX_MEMORY_TYPE_HOST);
#elif VX_VERSION_1_0
                    status = vxAccessImagePatch($output, &rect, 0, &addressing, reinterpret_cast<void **>(&data), VX_WRITE_ONLY);
                    if (status == vx_status_e::VX_SUCCESS) {
                        status = vxCommitImagePatch($output, &rect, 0, &addressing, data);
                    }
#else
#error Conversion from OpenCV to OpenVX is not implemented for that versiong of OpenVX.
#endif
                }

                delete[] data;
            }
        }
    } else {
        status = vx_status_e::VX_FAILURE;
    }
    return status;
}
