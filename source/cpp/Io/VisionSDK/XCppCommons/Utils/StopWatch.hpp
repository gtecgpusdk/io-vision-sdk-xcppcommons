/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_XCPPCOMMONS_UTILS_STOPWATCH_HPP_
#define IO_VISIONSDK_XCPPCOMMONS_UTILS_STOPWATCH_HPP_

/**
 * This class provides stopwatch functionality.
 */
class Io::VisionSDK::XCppCommons::Utils::StopWatch {
 public:
    /**
     * Start measurement.
     */
    void Start();

    /**
     * Stop measurement.
     */
    void Stop();

    /**
     * Stops running measurement and starts new one.
     * @return Returns elapsed time or zero if not started before.
     */
    long Restart();

    /**
     * @return Returns elapsed time. Note that measurement has to be stopped before, otherwise returns zero.
     */
    long getElapsed();

 private:
    long getCurrentTs();

    long startTime = 0;
    long elapsed = 0;
};

#endif  // IO_VISIONSDK_XCPPCOMMONS_UTILS_STOPWATCH_HPP_
