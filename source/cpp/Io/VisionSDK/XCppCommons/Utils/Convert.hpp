/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_XCPPCOMMONS_UTILS_CONVERT_HPP_
#define IO_VISIONSDK_XCPPCOMMONS_UTILS_CONVERT_HPP_

/**
 * This class provides API to convert between image formats OpenVX and OpenCV.
 */
class Io::VisionSDK::XCppCommons::Utils::Convert {
 public:
    /**
     * Convert OpenVX image to OpenCV image. Converts only to Opencv CV_8UC1 image format from VX_DF_IMAGE_U8 or VX_DF_IMAGE_S16
     * @param $input Specify vx_image data.
     * @param $output Specify reference for output image.
     * @param $context Specify vx_context owner.
     * @return Returns vx_status.
     */
    static vx_status VxToCv(const vx_image &$input, cv::Mat &$output, const vx_context &$context);

    /**
     * Convert OpenCV image to OpenVX image. Converts only from CV_8UC1 to VX_DF_IMAGE_U8 or VX_DF_IMAGE_S16.
     * @param $input Specify input image.
     * @param $output Specify reference for output image. Referenced pointer will be replaced by new one if $copy is false.
     * @param $context Specify vx_context owner.
     * @param $copy Specify true to force copy data from $input buffer to $output buffer, false by default.
     * @return Returns vx_status.
     */
    static vx_status CvToVx(const cv::Mat &$input, vx_image &$output, const vx_context &$context, bool $copy = false);
};

#endif  // IO_VISIONSDK_XCPPCOMMONS_UTILS_CONVERT_HPP_
