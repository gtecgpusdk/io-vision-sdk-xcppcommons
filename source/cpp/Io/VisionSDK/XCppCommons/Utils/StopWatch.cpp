/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <chrono>

#include "../sourceFilesMap.hpp"

using Io::VisionSDK::XCppCommons::Utils::StopWatch;

void StopWatch::Start() {
    this->elapsed = 0;
    this->startTime = this->getCurrentTs();
}

void StopWatch::Stop() {
    if (this->startTime > 0) {
        this->elapsed = this->getCurrentTs() - this->startTime;
    } else {
        this->elapsed = 0;
    }
}

long StopWatch::Restart() {
    this->Stop();
    auto retVal = this->getElapsed();
    this->Start();
    return retVal;
}

long StopWatch::getElapsed() {
    return this->elapsed;
}

long StopWatch::getCurrentTs() {
    return static_cast<long>(std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()).count());
}
