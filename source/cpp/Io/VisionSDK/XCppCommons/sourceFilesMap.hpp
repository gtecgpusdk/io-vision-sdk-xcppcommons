/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_XCPPCOMMONS_SOURCEFILESMAP_HPP_
#define IO_VISIONSDK_XCPPCOMMONS_SOURCEFILESMAP_HPP_

#include "../../../reference.hpp"

#endif  // IO_VISIONSDK_XCPPCOMMONS_SOURCEFILESMAP_HPP_
