/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_XCPPCOMMONS_PRIMITIVES_PATTERN_HPP_
#define IO_VISIONSDK_XCPPCOMMONS_PRIMITIVES_PATTERN_HPP_

/**
 * This class holds matrix data. Data could be initialized automatically from initializer list, cv::Mat, 2D vector
 * or filled with random data.
 */
template<typename T, typename _Alloc /*= std::allocator<T>*/>
class Io::VisionSDK::XCppCommons::Primitives::Pattern : protected std::_Vector_base<std::vector<T>, _Alloc> {
 public:
    Pattern() = default;

    explicit Pattern(const cv::Mat &$list) {
        this->intern = $list;
    }

    Pattern(int $width, int $height, T $min = 0, T $max = 0) {
        std::vector<std::vector<T>> data = {};
        if ($width > 0 && $height > 0) {
            std::random_device rd;
            std::mt19937 mt(rd());
            std::uniform_real_distribution<> dist(0, 1);

            auto calc = [&](T $tMin, T $tMax) -> T {
                return (T)(dist(mt) * (static_cast<double>($tMax) - $tMin) + $tMin);
            };

            auto generate = [&](T $tMin, T $tMax) {
                for (int j = 0; j < $width; j++) {
                    data.emplace_back(std::vector<T>());
                    for (int i = 0; i < $height; i++) {
                        data[j].emplace_back(calc($tMin, $tMax));
                    }
                }
            };

            if ($min < $max) {
                generate($min, $max);
            } else {
                generate(std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
            }
        } else {
            throw std::runtime_error("Pattern width && hight must be greater than 0.");
        }

        this->toCvMat(data);
    }

    Pattern(const std::initializer_list<std::vector<T>> &$list) {
        this->toCvMat({$list.begin(), $list.end()});
    }

    explicit Pattern(const std::vector<std::vector<T>> &$data) {
        this->toCvMat($data);
    }

    Pattern(const Pattern &$other) {
        this->toCvMat($other.fromCvMat());
    }

    Pattern &operator=(const std::initializer_list<std::vector<T>> &$list) {
        this->toCvMat({$list.begin(), $list.end()});
        return *this;
    }

    Pattern &operator=(const std::vector<std::vector<T>> &$data) {
        this->toCvMat($data);
        return *this;
    }

    Pattern &operator=(const cv::Mat &$mat) {
        this->intern = $mat;
        return *this;
    }

    Pattern &operator=(const Pattern &$other) {
        this->toCvMat($other.fromCvMat());
        return *this;
    }

    Pattern &operator=(Pattern &&$other) noexcept = default;

    bool operator==(const Pattern &$rhs) const {
        return this->compareTo($rhs.intern);
    }

    bool operator!=(const Pattern &rhs) const {
        return !(rhs == *this);
    }

    bool operator==(const cv::Mat &$rhs) const {
        return this->compareTo($rhs);
    }

    bool operator!=(const cv::Mat &rhs) const {
        return !(rhs == *this);
    }

    Pattern &operator*(double $y) {
        this->intern *= $y;
        return *this;
    }

    Pattern &operator*=(double $y) {
        this->intern *= $y;
        return *this;
    }

    Pattern &operator/(double $y) {
        this->intern /= $y;
        return *this;
    }

    Pattern &operator/=(double $y) {
        this->intern /= $y;
        return *this;
    }

    int getWidth() const {
        return this->intern.cols;
    }

    vx_uint32 getVxWidth() const {
        return (vx_uint32)this->getWidth();
    }

    int getHeight() const {
        return this->intern.rows;
    }

    vx_uint32 getVxHeight() const {
        return (vx_uint32)this->getHeight();
    }

    cv::Size getSize() const {
        return this->intern.size();
    }

    int getType() const {
        return this->intern.type();
    }

    string toString() const {
        string retVal = "[";
        for (const auto &row : this->fromCvMat()) {
            if (retVal.size() > 1) {
                retVal += "  ";
            }
            retVal += this->toString(row, ", ") + ";\n";
        }
        retVal.erase(retVal.size() - 2, 2);
        return retVal + "]";
    }

    const cv::Mat &getMat() const {
        return this->intern;
    }

    template<typename TN>
    Pattern <TN> CastTo() const {
        return this->convertTo<TN>(false);
    }

    template<typename TN>
    Pattern <TN> ConvertTo() const {
        return this->convertTo<TN>();
    }

    friend std::ostream &operator<<(std::ostream &$os, const Pattern &$pattern) {
        $os << $pattern.toString();
        return $os;
    }

 private:
    void toCvMat(const std::vector<std::vector<T>> &$data) {
        this->intern.release();

        if (!$data.empty() && !$data[0].empty()) {
            unsigned rows = $data.size(), cols = $data[0].size();
            int type = 0;

            if (typeid(T) == typeid(char) ||   // NOLINT(readability/casting)
                typeid(T) == typeid(uint8_t) ||
                typeid(T) == typeid(int8_t)) {
                type = CV_8UC1;
            } else if (typeid(T) == typeid(uint16_t)) {
                type = CV_16UC1;
            } else if (typeid(T) == typeid(int16_t)) {
                type = CV_16SC1;
            } else if (typeid(T) == typeid(unsigned int) ||
                       typeid(T) == typeid(int) ||  // NOLINT(readability/casting)
                       typeid(T) == typeid(int32_t) ||
                       typeid(T) == typeid(uint32_t)) {
                type = CV_32SC1;
            } else if (typeid(T) == typeid(float)) {
                type = CV_32FC1;
            } else if (typeid(T) == typeid(double)) {
                type = CV_64FC1;
            } else {
                throw std::runtime_error((string("Type name \"") + typeid(T).name() + "\" is not supported.").c_str());
            }

            this->intern.create(rows, cols, type);
            for (unsigned j = 0; j < rows; j++) {
                for (unsigned i = 0; i < cols; i++) {
                    this->intern.at<T>(j, i) = $data[j][i];
                }
            }
        }
    }

    std::vector<std::vector<T>> fromCvMat() const {
        std::vector<std::vector<T>> output{};
        for (int j = 0; j < this->intern.rows; j++) {
            output.emplace_back(std::vector<T>());
            for (int i = 0; i < this->intern.cols; i++) {
                output[j].emplace_back(this->intern.at<T>(j, i));
            }
        }
        return output;
    }

    string toString(const std::vector<T> &$data, const string &sep = " ") const {
        return std::accumulate($data.begin(), $data.end(), string(), [&](const string &a, const T &b) {
            string retVal;
            if (a.empty()) {
                retVal = std::to_string(b);
            } else {
                retVal = a + sep + std::to_string(b);
            }
            return retVal;
        });
    }

    bool compareTo(const cv::Mat &$other) const {
        auto cmp = [](const cv::Mat &$a, const cv::Mat &$b) -> bool {
            cv::Mat diff;
            cv::compare($a, $b, diff, cv::CMP_NE);
            return cv::countNonZero(diff) == 0;
        };

        bool status = false;
        if (this->intern.size() == $other.size() && this->intern.depth() == $other.depth()) {
            if (this->intern.channels() == $other.channels()) {
                if ($other.channels() == 1) {
                    status = cmp(this->intern, $other);
                } else {
                    std::vector<cv::Mat> vectA, vectB;
                    cv::split(this->intern, vectA);
                    cv::split($other, vectB);
                    for (std::size_t i = 0; i < vectA.size(); i++) {
                        status = cmp(vectA[i], vectB[i]);
                        if (!status) {
                            break;
                        }
                    }
                }
            }
        }

        return status;
    }

    template<typename TN>
    Pattern <TN> convertTo(bool $round = true, bool $saturate = true) const {
        cv::Mat output;
        int outType;
        auto cast = [&](double $value) -> TN {
            double tmp = std::floor($value);
            if ($saturate) {
                if (tmp > std::numeric_limits<TN>::max()) {
                    tmp = std::numeric_limits<TN>::max();
                } else if (tmp < std::numeric_limits<TN>::min()) {
                    tmp = std::numeric_limits<TN>::min();
                }
            }
            return static_cast<TN>(tmp);
        };
        if ((typeid(TN) == typeid(char)) || (typeid(TN) == typeid(int8_t))) {
            outType = CV_8S;
        } else if (typeid(TN) == typeid(uint8_t)) {
            outType = CV_8U;
        } else if (typeid(TN) == typeid(int16_t)) {
            outType = CV_16S;
        } else if (typeid(TN) == typeid(uint16_t)) {
            outType = CV_16U;
        } else if ((typeid(TN) == typeid(int)) || (typeid(TN) == typeid(int32_t)) || (typeid(TN) == typeid(uint32_t)) ||
                   (typeid(TN) == typeid(unsigned int)) || (typeid(TN) == typeid(unsigned))) {
            outType = CV_32S;
        } else if (typeid(TN) == typeid(float)) {
            outType = CV_32F;
        } else if (typeid(TN) == typeid(double)) {
            outType = CV_64F;
        } else {
            throw std::runtime_error("Pattern can not be converted to type: " + string(typeid(TN).name()));
        }

        if ($round) {
            this->intern.convertTo(output, outType);
        } else {
            output.create(this->intern.size(), outType);
            for (int j = 0; j < this->intern.rows; j++) {
                for (int i = 0; i < this->intern.cols; i++) {
                    output.at<TN>(j, i) = cast(this->intern.at<T>(j, i));
                }
            }
        }

        return Pattern<TN>(output);
    }

    cv::Mat intern{};
};

#endif  // IO_VISIONSDK_XCPPCOMMONS_PRIMITIVES_PATTERN_HPP_
