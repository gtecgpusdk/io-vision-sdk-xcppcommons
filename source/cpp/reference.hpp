/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_VISIONSDK_XCPPCOMMONS_HPP_  // NOLINT
#define IO_VISIONSDK_XCPPCOMMONS_HPP_

#include <string>
#include <iostream>
#include <vector>
#include <functional>
#include <random>

#include <VX/vx.h>  // NOLINT(build/include_order)

#include <opencv2/highgui/highgui.hpp>  // NOLINT(build/include_order)
#include <opencv2/imgproc/imgproc.hpp>  // NOLINT(build/include_order)

// global-using-start
using std::string;
// global-using-stop

#include "interfacesMap.hpp"
#include "Io/VisionSDK/XCppCommons/sourceFilesMap.hpp"

// generated-code-start
#include "Io/VisionSDK/XCppCommons/Primitives/Pattern.hpp"
#include "Io/VisionSDK/XCppCommons/Utils/Convert.hpp"
#include "Io/VisionSDK/XCppCommons/Utils/StopWatch.hpp"
// generated-code-end

#endif  // IO_VISIONSDK_XCPPCOMMONS_HPP_  NOLINT
