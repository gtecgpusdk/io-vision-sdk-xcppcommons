/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_VISIONSDK_XCPPCOMMONS_INTERFACESMAP_HPP_  // NOLINT
#define IO_VISIONSDK_XCPPCOMMONS_INTERFACESMAP_HPP_

namespace Io {
    namespace VisionSDK {
        namespace XCppCommons {
            namespace Primitives {
                template<typename T, typename _Alloc  = std::allocator<T>> class Pattern;
            }
            namespace Utils {
                class Convert;
                class StopWatch;
            }
        }
    }
}

#endif  // IO_VISIONSDK_XCPPCOMMONS_INTERFACESMAP_HPP_  // NOLINT
