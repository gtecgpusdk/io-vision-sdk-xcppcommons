Release Name: io-vision-sdk-xcppcommons v2018.0.0

io-vision-sdk-xcppcommons
Description: Commons classes for the Vision SDK cross-compile projects written in C++.
Author: NXP
License: BSD-3-Clause. See LICENSE.txt
Format: source code and libraries
Location: resource/libs/VisionSDK/[platform]/libs/kernels

openvx v1.1
Description: An open, royalty-free standard API for cross platform acceleration of computer vision applications.
Author: The Khronos Group Inc.
License: MIT. See LICENSE
Format: compiled source code and header files
Location: resource/libs/VisionSDK/[platform]/bins,
          resource/libs/VisionSDK/[platform]/libs/openvx

opencv v2.4.13.4
Description: Library of programming functions mainly aimed at real-time computer vision.
Author: Intel Corporation, Willow Garage, Itseez
License: BSD-3-Clause. See LICENSE
Format: compiled source code and header files
Location: resource/libs/VisionSDK/[platform]/bins,
          resource/libs/VisionSDK/[platform]/libs/opencv
